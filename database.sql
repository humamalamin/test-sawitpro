/**
  This is the SQL script that will be used to initialize the database schema.
  We will evaluate you based on how well you design your database.
  1. How you design the tables.
  2. How you choose the data types and keys.
  3. How you name the fields.
  In this assignment we will use PostgreSQL as the database.
  */
/** This is test table. Remove this table and replace with your own tables. */
CREATE TABLE IF NOT EXISTS users (
	id serial PRIMARY KEY,
	name VARCHAR (150) UNIQUE NOT NULL,
  phone VARCHAR(15) NOT NULL,
  password VARCHAR(100) NOT NULL,
  count_login int DEFAULT 0,
  created_at timestamp NOT NULL DEFAULT now(),
  updated_at timestamp NULL DEFAULT NOW()
);

-- SAMPLE DATA
INSERT INTO users(name,phone,password)VALUES("humam", "8589192122","5e20ef49afd74e7e67bc96f4a4b779003b55d479");