// This file contains types that are used in the repository layer.
package repository

type RequestRegister struct {
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Password string `json:"password"`
}

type RequestLogin struct {
	Phone    string `json:"phone"`
	Password string `json:"password"`
}

type User struct {
	ID       int    `json:"-"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Password string `json:"-"`
}
