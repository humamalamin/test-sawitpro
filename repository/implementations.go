package repository

import (
	"context"
	"errors"
)

func (r *Repository) Register(ctx context.Context, input *RequestRegister) (int, error) {
	id := 0
	sqlStatement := `INSERT INTO users (name,phone,password) VAlUES($1,$2,$3) RETURNING id`
	err := r.Db.QueryRow(sqlStatement, input.Name, input.Phone, input.Password).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (r *Repository) Login(ctx context.Context, input *RequestLogin) (User, error) {
	var userResp User
	sqlStatement := `SELECT id,name,phone,password FROM users WHERE phone = $1`
	err := r.Db.QueryRow(sqlStatement, input.Phone).Scan(&userResp.ID, &userResp.Name, &userResp.Phone, &userResp.Password)
	if err != nil {
		return userResp, err
	}

	err = r.countLogin(userResp.ID, 1)
	if err != nil {
		return userResp, err
	}

	return userResp, nil
}

func (r *Repository) Profile(ctx context.Context, id int) (User, error) {
	var userResp User
	sqlStatement := `SELECT name,phone FROM users WHERE id = $1`
	err := r.Db.QueryRow(sqlStatement, id).Scan(&userResp.Name, &userResp.Phone)
	if err != nil {
		return userResp, err
	}

	return userResp, nil
}

func (r *Repository) UpdateProfile(ctx context.Context, input *User) error {
	var count int64
	sqlStatement := `SELECT COUNT(phone) as count FROM users WHERE phone = $1 AND id != $2`
	err := r.Db.QueryRow(sqlStatement, input.Phone, input.ID).Scan(&count)
	if err != nil {
		return err
	}

	if count > 0 {
		return errors.New("409")
	}

	sqlStatement2 := `UPDATE users SET name=$1, phone=$2 WHERE id=$3`
	_, err = r.Db.Exec(sqlStatement2, input.Name, input.Phone, input.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) countLogin(id, numb int) error {
	var countLogin int
	sqlStatement := `SELECT count_login FROM users WHERE id = $1`
	err := r.Db.QueryRow(sqlStatement, id).Scan(&countLogin)
	if err != nil {
		return err
	}
	total := countLogin + numb

	sqlStatement2 := `UPDATE users SET count_login=$1 WHERE id=$2`
	_, err = r.Db.Exec(sqlStatement2, total, id)
	if err != nil {
		return err
	}

	return nil
}
