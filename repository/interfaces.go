// This file contains the interfaces for the repository layer.
// The repository layer is responsible for interacting with the database.
// For testing purpose we will generate mock implementations of these
// interfaces using mockgen. See the Makefile for more information.
package repository

import "context"

type RepositoryInterface interface {
	Register(ctx context.Context, input *RequestRegister) (int, error)
	Login(ctx context.Context, input *RequestLogin) (User, error)
	Profile(ctx context.Context, id int) (User, error)
	UpdateProfile(ctx context.Context, input *User) error
}
