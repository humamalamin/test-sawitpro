package handler

import (
	validationHelper "github.com/SawitProRecruitment/UserService/helper/validation"
)

type (
	RegisterRequest struct {
		Name     string `json:"name" validate:"required,min=3,max=60"`
		Phone    string `json:"phone" validate:"required,min=10,max=13"`
		Password string `json:"password" validate:"required,min=6,max=64"`
	}
)

func (r RegisterRequest) ValidateRegister() error {
	err := validationHelper.ValidatePhoneNumber(r.Phone)
	if err != nil {
		return err
	}

	err = validationHelper.ValidatePassword(r.Password)
	if err != nil {
		return err
	}

	return nil
}

type LoginRequest struct {
	Phone    string `json:"phone" validate:"required,min=10,max=13"`
	Password string `json:"password" validate:"required,min=6,max=64"`
}

func (r LoginRequest) ValidateLogin() error {
	err := validationHelper.ValidatePhoneNumber(r.Phone)
	if err != nil {
		return err
	}

	err = validationHelper.ValidatePassword(r.Password)
	if err != nil {
		return err
	}

	return nil
}

type UpdateProfileRequest struct {
	Name  string `json:"name" validate:"required,min=3,max=60"`
	Phone string `json:"phone" validate:"required,min=10,max=13"`
}

func (r UpdateProfileRequest) ValidateupdateProfile() error {
	err := validationHelper.ValidatePhoneNumber(r.Phone)
	if err != nil {
		return err
	}

	return nil
}
