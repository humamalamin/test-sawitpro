package handler

import (
	"net/http"
	"os"
	"time"

	jwtAuth "github.com/SawitProRecruitment/UserService/helper/auth"
	"github.com/SawitProRecruitment/UserService/helper/conv"
	"github.com/SawitProRecruitment/UserService/repository"
	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
)

// (POST /register)
func (s *Server) Register(ctx echo.Context) error {
	var req RegisterRequest
	var resp DefaultResponse

	resp.Message = "Register successfully"

	if err := ctx.Bind(&req); err != nil {
		return err
	}

	err := req.ValidateRegister()
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	req.Phone = req.Phone[3:]
	if err := ctx.Validate(&req); err != nil {
		return err
	}

	password := conv.HashShaPassword(req.Password, os.Getenv("PASSWORD_SALT"))
	reqEntity := repository.RequestRegister{
		Name:     req.Name,
		Phone:    req.Phone,
		Password: password,
	}

	id, err := s.Repository.Register(ctx.Request().Context(), &reqEntity)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	resp.Data = id
	return ctx.JSON(http.StatusOK, resp)
}

func (s *Server) Login(ctx echo.Context) error {
	var req LoginRequest
	var resp DefaultResponse

	resp.Message = "Login successfully"

	if err := ctx.Bind(&req); err != nil {
		return err
	}

	err := req.ValidateLogin()
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}
	req.Phone = req.Phone[3:]
	if err := ctx.Validate(&req); err != nil {
		return err
	}

	password := conv.HashShaPassword(req.Password, os.Getenv("PASSWORD_SALT"))
	reqEntity := repository.RequestLogin{
		Phone:    req.Phone,
		Password: password,
	}

	result, err := s.Repository.Login(ctx.Request().Context(), &reqEntity)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	hash, _ := conv.HasPassword(password)
	match := conv.CheckPasswordHash(result.Password, hash)
	if !match {
		resp.Message = "Password not valid"
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	jwtData := &jwtAuth.JwtCustomClaims{
		ID: int64(result.ID),
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 72)),
		},
	}

	token, err := jwtAuth.NewJwt().GenerateToken(jwtData)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	resp.Data = echo.Map{"token": token}
	return ctx.JSON(http.StatusOK, resp)
}

func (s *Server) Profile(ctx echo.Context) error {
	var resp DefaultResponse

	resp.Message = "Fetch profile successfully"
	userInfo, err := jwtAuth.NewJwt().GetUserInformationContext(ctx)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusForbidden, resp)
	}

	res, err := s.Repository.Profile(ctx.Request().Context(), userInfo.ID)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusForbidden, resp)
	}

	res.Phone = "+62" + res.Phone

	return ctx.JSON(http.StatusOK, res)
}

func (s *Server) UpdateProfile(ctx echo.Context) error {
	var resp DefaultResponse
	var req UpdateProfileRequest

	resp.Message = "Update profile successfully"
	userInfo, err := jwtAuth.NewJwt().GetUserInformationContext(ctx)
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusForbidden, resp)
	}

	if err := ctx.Bind(&req); err != nil {
		return err
	}

	err = req.ValidateupdateProfile()
	if err != nil {
		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	req.Phone = req.Phone[3:]
	if err := ctx.Validate(&req); err != nil {
		return err
	}

	reqEntity := repository.User{
		Name:  req.Name,
		Phone: req.Phone,
		ID:    userInfo.ID,
	}

	err = s.Repository.UpdateProfile(ctx.Request().Context(), &reqEntity)
	if err != nil {
		if err.Error() == "409" {
			resp.Message = "Phone number is duplicate"
			return ctx.JSON(http.StatusConflict, resp)
		}

		resp.Message = err.Error()
		return ctx.JSON(http.StatusBadRequest, resp)
	}

	return ctx.JSON(http.StatusOK, resp)
}
