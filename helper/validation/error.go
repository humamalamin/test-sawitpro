package validation

import (
	"errors"
)

var (
	ErrorInvalidPhoneNumber = errors.New("invalid phone number format")
	ErrorInvalidPassword    = errors.New("invalid password format")
)
