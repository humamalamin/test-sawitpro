package validation

import (
	"regexp"
)

func ValidatePhoneNumber(data string) error {
	r, _ := regexp.Compile(RegexPhoneNumber)

	res := r.MatchString(data)
	if !res {
		return ErrorInvalidPhoneNumber
	}

	return nil
}

func ValidatePassword(data string) error {
	r, err := regexp.Compile(RegexPassword)
	if err != nil {
		return nil
	}

	res := r.MatchString(data)
	if !res {
		return ErrorInvalidPassword
	}

	return nil
}
