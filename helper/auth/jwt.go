package authHelper

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
)

type ContextKey string

type Jwt interface {
	GenerateToken(data *JwtCustomClaims) (string, error)
	VerifyAccessToken(token string) (*JwtCustomClaims, error)
	GetUserInformationContext(r echo.Context) (*UserData, error)
}

type Options struct {
	signinKey string
}

// GenerateToken implements Jwt.
func (o *Options) GenerateToken(data *JwtCustomClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, data)
	t, err := token.SignedString([]byte(o.signinKey))
	if err != nil {
		return "", err
	}
	return t, nil
}

// VerifyAccessToken implements Jwt.
func (o *Options) VerifyAccessToken(token string) (*JwtCustomClaims, error) {
	parsedToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method invalid")
		}

		return []byte(o.signinKey), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok || !parsedToken.Valid {
		return nil, err
	}

	jwtData := &JwtCustomClaims{
		ID: int64(claims["id"].(float64)),
	}

	return jwtData, nil
}

func (o *Options) ExtractToken(r echo.Context) (string, error) {
	authHeader := r.Request().Header.Get("Authorization")
	if authHeader == "" {
		return "", errors.New("invalid Access Token")
	}

	accessToken := strings.Split(authHeader, " ")
	if accessToken[0] != "Bearer" {
		return "", errors.New("authorization header doesn't have bearer format")
	}

	if len(accessToken) == 1 {
		return "", errors.New("authorization header doesn't have access token value")
	}

	return accessToken[1], nil
}

func (o *Options) GetUserInformationContext(r echo.Context) (*UserData, error) {
	accessToken, err := o.ExtractToken(r)
	if err != nil {
		return nil, err
	}
	jwtData, err := o.VerifyAccessToken(accessToken)
	if err != nil {
		return nil, err
	}

	if jwtData == nil {
		return nil, errors.New("failed getting user info from context")
	}

	userData := &UserData{
		ID: int(jwtData.ID),
	}

	return userData, nil
}

func NewJwt() Jwt {
	opt := new(Options)
	opt.signinKey = os.Getenv("JWT_SIGNING_KEY")

	return opt
}
