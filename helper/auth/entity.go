package authHelper

import "github.com/golang-jwt/jwt/v5"

type JwtCustomClaims struct {
	ID int64 `json:"id"`
	jwt.RegisteredClaims
}

type Middleware struct{}

type CustomErrResponse struct {
	ErrCode string `json:"err_code"`
	Message string `json:"message"`
}

type UserData struct {
	ID int `json:"id"`
}
